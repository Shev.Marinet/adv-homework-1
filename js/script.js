document.addEventListener('click', function (e) {
    let target = e.target;
    if (target.classList.contains('navigation__list-links-link')) {
        document.querySelectorAll('.navigation__list-links-link').forEach(function (link) {
           link.classList.remove('active');
        });
        target.classList.toggle('active');
    }
    while (target !== document){
        if (target.classList.contains('navigation__menu')) {
            document.querySelectorAll('.navigation__menu-line').forEach(function (line) {
                line.classList.toggle('animate');
            });
            document.querySelector('.navigation__list').classList.toggle('active');
        }
        target=target.parentNode;
    }
});